"
"   ┏━━┓┏┓┏┓┏┳━━━┓
"   ┃┏┓┃┃┃┃┃┃┃┏━━┛
"   ┃┗┛┗┫┃┃┃┃┃┗━━┓
"   ┃┏━┓┃┗┛┗┛┃┏━━┛
"   ┃┗━┛┣┓┏┓┏┫┗━━┓   Brian Earley
"   ┗━━━┛┗┛┗┛┗━━━┛   .vimrc
"

" Turn on line numbering and relative numbering
set number
set rnu

" Indentation settings
set autoindent
" Show existing tab with 4 spaces
set tabstop=4
" When indenting with '>', use 4 spaces
set shiftwidth=4
" Insert 4 spaces on pressing Tab
set expandtab

" Syntax highlighting
syntax on

" Cursorline highlights the current line and line number
set cursorline
" Disable cursorline's line highlight
highlight clear CursorLine
"Keep cursorline's line number highlighting
highlight CursorLineNR ctermbg=red

" Highlights current line
"set cursorcolumn

" Highlight search results
set hlsearch

" Print line number in :hardcopy
set printoptions=number:y,header:2,syntax:y,left:4pc,right:4pc,bottom:2pc,top:10pc

" Assign syntax highlighting based on extensions
"utocmd BufNewFile,BufRead *.f90,*.f,*.for,*.input,*.output,*.dat set filetype=fortran
autocmd BufNewFile,BufRead *.std set syntax=csv
autocmd BufNewFile,BufRead *.F   set syntax=fortran
autocmd BufNewFile,BufRead *.F90 set syntax=fortran
autocmd BufNewFile,BufRead *.dat set syntax=fortran
autocmd BufNewFile,BufRead *.DAT set syntax=fortran
autocmd BufNewFile,BufRead *.cmn set syntax=fortran
autocmd BufNewFile,BufRead *.CMN set syntax=fortran
autocmd BufNewFile,BufRead *.input set syntax=fortran
autocmd BufNewFile,BufRead *.INPUT set syntax=fortran

autocmd BufNewFile,BufRead make.* set syntax=make
autocmd BufNewFile,BufRead make-* set syntax=make

autocmd BufNewFile,BufRead runit* set syntax=bash

autocmd BufNewFile,BufRead *.f90 set shiftwidth=2
autocmd BufNewFile,BufRead *.f   set shiftwidth=2

" Try to reopen file at last selected line
autocmd BufReadPost *
      \ if line("'\"") > 0 && line("'\"") <= line("$") |
      \ exe "normal! g`\"" |
      \ endif

set ignorecase

set wrapmargin=0
set textwidth=0
set nowrap

set wildmenu
set wildmode=list,longest,full

set laststatus=2

" Don't start with folds (important for plot.inp files)
set foldlevelstart=1

" Allows proper reindentation of Fortran files with gg=G
let fortran_do_enddo=1

" Fix gf in bash scripts
set isfname-==
"nnoremap gf ^f/gf

" Check file type of new files between 20 and 200 characters entered
function CheckFileType()
  if exists("b:countCheck") == 0
    let b:countCheck = 0
  endif
  let b:countCheck += 1
  " Don't start detecting until approx. 20 characters
  if &filetype == "" && b:countCheck > 20 && b:countCheck < 200
    filetype detect
  " If we've exceeded the count threshold (200), OR a filetype
  " has been detected, delete the autocmd!
  elseif b:countCheck >= 200 || &filetype != ""
    autocmd! newFileDetection
  endif
endfunction
augroup newFileDetection
autocmd CursorMovedI * call CheckFileType()
augroup END

" Use 24-bit (true color) mode in Vim when outside tmux
" IF you're using tmux version 2.2 or later, you can remove the outermost
" $TMUX check and use tmux's 24-bit instead
"if (empty($TMUX))
  if (has("nvim"))
    " For Neovim 0.1.3 and 0.1.4 < https://github.com/neovim/neovim/pull/2198 >
    let $NVIM_TUI_ENABLE_TRUE_COLOR=1
  endif
  if (has("termguicolors"))
    set termguicolors
  endif
"endif

"
" Leader key mappings
"
let mapleader = " "
" Hide trailing characters
nnoremap <Leader>h :TrailerHide<CR>
" Delete trailing characters
nnoremap <Leader>s :%s/\s\+$//e
" Move any lines starting with a ! to column 0 (use after automatic reindentation)
nnoremap <Leader>e :%s/\v^[ ]+!/!/ge
" Move any lines starting with a # to column 0 (use after automatic reindentation)
nnoremap <Leader>p :%s/\v^[ ]+#/#/ge
" Open file tree
nnoremap <Leader>t :NERDTreeToggle<CR>

" Automatically rebalance windows on Vim resize
autocmd VimResized * :wincmd =

" Save undo trees in files
set undofile
set undodir=~/.vim/undo

" Number of undo saved
set undolevels=10000

" Try to stay in center of the screen
"set scrolloff=999

" Use the mouse
if has('mouse')
  set mouse=a
endif

" Easier buffer navigation
nnoremap H :bp<CR>
nnoremap L :bn<CR>
nnoremap <C-h> <C-w>h
nnoremap <C-l> <C-w>l
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k

" Recenter on Ctrl-D/U
nnoremap <C-d> <C-d>zz
nnoremap <C-u> <C-u>zz

" Better vim motions
nnoremap <silent> k gk
nnoremap <silent> j gj
nnoremap <silent> 0 g0
nnoremap <silent> $ g$

"autocmd vimenter * let &shell='/dsk9/home/brian/z/bin/zsh -i'

" Color Scheme Configuration
" colorscheme onedark
" colorscheme elflord
" colorscheme deus
" colorscheme molokai
" colorscheme gruvbox
" colorscheme catppuccin_macchiato
silent! colorscheme catppuccin_mocha
set background=dark

"highlight MatchParen gui=bold guibg=NONE guifg=lightblue cterm=bold ctermbg=NONE

" In visual mode, drag and reindent selected text with Shift-J, Shift-K
vnoremap J :m '>+1<CR>gv=gv
vnoremap K :m '<-2<CR>gv=gv

" Re-center screen on search results
nnoremap n nzzzv
nnoremap N Nzzzv

" Disable F1, Q keys
map <F1> <Nop>
map Q <Nop>
