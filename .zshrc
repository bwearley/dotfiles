#
#   ┏━━┓┏┓┏┓┏┳━━━┓
#   ┃┏┓┃┃┃┃┃┃┃┏━━┛
#   ┃┗┛┗┫┃┃┃┃┃┗━━┓
#   ┃┏━┓┃┗┛┗┛┃┏━━┛
#   ┃┗━┛┣┓┏┓┏┫┗━━┓   Brian Earley
#   ┗━━━┛┗┛┗┛┗━━━┛   .zshrc
#
#zmodload zsh/zprof

#export PATH="/Users/brian/anaconda3/bin:$PATH"
# source ~/opt/anaconda3/etc/profile.d/conda.sh  # commented out by conda initialize
# conda activate base  # commented out by conda initialize

export PATH="$HOME/bin/:$PATH"

HISTFILE=~/.histfile
HISTSIZE=10000
SAVEHIST=10000
setopt extendedglob
setopt SHARE_HISTORY HIST_IGNORE_DUPS
bindkey -v

autoload -Uz compinit
compinit

export EDITOR=vim
export GIT_EDITOR=vim

# Keybinds
autoload -U edit-command-line
zle -N edit-command-line
bindkey '^xe'  edit-command-line
bindkey '^x^e' edit-command-line
bindkey '^[[H' beginning-of-line
bindkey '^[[1~' beginning-of-line  #tmux
bindkey '^[[F' end-of-line
bindkey '^[[4~' end-of-line #tmux
bindkey '^[[3~' delete-char #tmux
bindkey '^R' history-incremental-search-backward
bindkey '^[[A' history-search-backward
bindkey '^[[B' history-search-forward

bindkey '^ ' forward-word   #Ctrl+Space to autocomplete next word

#bindkey -s '^[[20~' 'clear^M ^Br'   #F9 # doesn't work
bindkey -s '^[[21~' 'tmux^M'                #F10
#bindkey -s '^[[23~' 'run.sh rm; run.sh^M'   #F11
#bindkey -s '^[[24~' 'rm *.o^M rm *.mod^M'   #F12

# Case insensitive searching BUT prefer case-sensitive where applicable
zstyle ':completion:*' matcher-list '' 'm:{a-zA-Z}={A-Za-z}' 'r:|=*' 'l:|=* r:|=*'
autoload -Uz compinit && compinit
#bindkey -s '^[OQ' "exit^M exit^M exit^M" # Testing to try to exit both tmux and the terminal

# Prompt customization
CHECK='✓'
ECKS='✕'
ARROW='→'
GREENCHECK="%F{green}${CHECK}%f"
REDECKS="%F{red}${ECKS}%f"

#PROMPT='%F{blue}%n%f@%F{magenta}%m%f %F{green}%4~%f $ '
#PROMPT="%F{blue}%n%f@%F{magenta}%m%f %F{cyan}%4~%f %(?.${GREENCHECK}.${REDECKS}) ${ARROW} "
PROMPT="%F{blue}%n%f@%F{red}%m%f %F{cyan}%~%f %(?.${GREENCHECK}.${REDECKS}) ${ARROW} "
RPROMPT="%F{yellow}%*%f"

# Background Prompt
# GREENCHECK="%K{green}${CHECK}%k"
# REDECKS="%K{red}${ECKS}%k"
# PROMPT="%K{blue}%n%k@%K{magenta}%m%k %K{cyan}%4~%k %(?.${GREENCHECK}.${REDECKS}) ${ARROW} "   #background version

alias bplot="~/Projects/bplot/src/bplot.py"

alias bt="btop"
alias cls="clear"
#alias lst="ls -laG"
#alias ll="ls -laG"
alias lst="exa -la --icons"
alias ll="exa -la --icons"
alias clst="clear; lst;"
alias cd..="cd .."
alias cd...="cd ..; cd ..;"
alias lst='exa -lg'
alias ll=lst
alias lstt='lst -sold -r'
alias clst="cls;lst"
alias clstt="cls;lstt"
alias nnn="nnn -dA"
alias rg="rg -i"
alias nf="neofetch"
alias xp="xpanes"

source /opt/intel/oneapi/setvars.sh

export aoc="/Users/brian/Dropbox/Projects/aoc/aoc/advent-of-code-2023"

export PATH="$HOME/bin/bin/:$PATH"

function cdaoc() {
  if [[ $1 != "" ]]; then
    day=$(printf "%02d" $1)
    cd $aoc/day$day
  else
    cd $aoc
  fi
}

function ptitle() {
    tmux set-option -p @custom_pane_title "$1"
}

#eval "$(oh-my-posh init zsh)"
#export NNN_PLUG='f:finder;o:fzopen;p:mocq;d:diffs;t:nmount;v:imgview'
export NNN_PLUG='p:preview-tui'
export SPLIT='v'
export NNN_FIFO=/tmp/nnn.fifo
n ()
{
    # Block nesting of nnn in subshells
    if [[ "${NNNLVL:-0}" -ge 1 ]]; then
        echo "nnn is already running"
        return
    fi

    # The behaviour is set to cd on quit (nnn checks if NNN_TMPFILE is set)
    # If NNN_TMPFILE is set to a custom path, it must be exported for nnn to
    # see. To cd on quit only on ^G, remove the "export" and make sure not to
    # use a custom path, i.e. set NNN_TMPFILE *exactly* as follows:
    #     NNN_TMPFILE="${XDG_CONFIG_HOME:-$HOME/.config}/nnn/.lastd"
    export NNN_TMPFILE="${XDG_CONFIG_HOME:-$HOME/.config}/nnn/.lastd"

    # Unmask ^Q (, ^V etc.) (if required, see `stty -a`) to Quit nnn
    # stty start undef
    # stty stop undef
    # stty lwrap undef
    # stty lnext undef

    # The backslash allows one to alias n to nnn if desired without making an
    # infinitely recursive alias
    \nnn "$@" -dAe

    if [ -f "$NNN_TMPFILE" ]; then
            . "$NNN_TMPFILE"
            rm -f "$NNN_TMPFILE" > /dev/null
    fi
}

function y() {
	local tmp="$(mktemp -t "yazi-cwd.XXXXX")"
	yazi "$@" --cwd-file="$tmp"
	if cwd="$(cat -- "$tmp")" && [ -n "$cwd" ] && [ "$cwd" != "$PWD" ]; then
		cd -- "$cwd"
	fi
	rm -f -- "$tmp"
}

function preexec() {
    #timer=$(($(date +%s%0N)/1000000)) #ms
    #timer=$(($(date +%s%3N))) #ms
    #timer=$(( $( date +%s%3N ) / 1000 )) #seconds
    timer=$(date +%s) #seconds
}

function precmd() {
    if [ $timer ]; then
        #now=$(($(date +%s%0N)/1000000)) #ms
        #now=$(($(date +%s%3N))) #ms
        #now=$(($(date +%s%3N)/1000)) #seconds
        now=$(date +%s) #seconds
        elapsed=$(($now-$timer))
        export RPROMPT="%F{yellow}(${elapsed}s) %*%f"
    fi
}

# Convert *>* to *.*
function accept-line-override() {
    BUFFER=${BUFFER/"*>*"/"*.*"}
    zle .accept-line
}
zle -N accept-line accept-line-override

export FZF_DEFAULT_OPTS='--height 40% --layout=reverse --border'
export FZF_CTRL_T_OPTS="--preview 'bat -n --color=always {}' --bind 'ctrl-/:change-preview-window(down|hidden|)'"
export FZF_ALT_C_OPTS="--preview 'tree -C {}'"

function ptitle() {
    tmux set-option -p @custom_pane_title "$1"
}

function pbg() {
    tmux select-pane -P "bg=$1"
}

source /opt/homebrew/share/zsh-autosuggestions/zsh-autosuggestions.zsh

source ~/.fzf-key-bindings.sh

source /opt/homebrew/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

# syntax-highlighting must be at the end of .zshrc
#source ~/.zsh/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
#zprof

# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
__conda_setup="$('/Users/brian/anaconda3/bin/conda' 'shell.zsh' 'hook' 2> /dev/null)"
if [ $? -eq 0 ]; then
    eval "$__conda_setup"
else
    if [ -f "/Users/brian/anaconda3/etc/profile.d/conda.sh" ]; then
        . "/Users/brian/anaconda3/etc/profile.d/conda.sh"
    else
        export PATH="/Users/brian/anaconda3/bin:$PATH"
    fi
fi
unset __conda_setup
# <<< conda initialize <<<


[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
